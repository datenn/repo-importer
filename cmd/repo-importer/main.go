package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

const (
	createRepoUrl    = "api/v1/organizations/{organizationId}/products"
	authenticateUrl  = "api/v1/login"
	defaultHost      = "http://localhost:8081"
	email            = ""
	password         = ""
	organizationUuid = "89a83011-acd9-4320-9cec-35d56fed6385"
)

type Credentials struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type TokenResponse struct {
	Token string `json:"idToken"`
}

type Repo struct {
	Version     		 string `json:"version"`
	Branch 				 string `json:"branch"`
	Name        		 string `json:"name"`
	Description 		 string `json:"description"`
	Repository  		 string `json:"repository"`
	LanguageVersion      string `json:"languageVersion"`
	CodeDirectory 	 	 string `json:"codeDirectory"`
}

type Repos struct {
	Repos []Repo `json:"repos"`
}

type Client struct {
	httpClient *http.Client
	host       string
}

func buildRequest(host string, method string, path string, pathParams map[string]string, body []byte, authorization string) (*http.Request, error) {
	fullPath, err := buildPath(host, path, pathParams, nil)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest(method, fullPath, bytes.NewReader(body))
	if err != nil {
		return nil, err
	}
	if len(authorization) > 0 {
		req.Header.Add("Authorization", authorization)
	}
	req.Header.Add("Content-Type", "application/json")
	return req, nil
}

func buildPath(host string, reqPath string, pathParams, queryParams map[string]string) (string, error) {
	urlPath, err := url.Parse(host)
	if err != nil {
		return "", err
	}
	pathWithParams := genPathParams(reqPath, pathParams)
	urlPath.Path = path.Join(urlPath.Path, pathWithParams)
	rawQuery := genQueryParams(queryParams)
	urlPath.RawQuery = rawQuery
	return urlPath.String(), nil
}

func genPathParams(basePath string, params map[string]string) string {
	if len(params) == 0 {
		return basePath
	}
	for k, v := range params {
		placeHolder := fmt.Sprintf("{%s}", k)
		basePath = strings.Replace(basePath, placeHolder, v, 1)
	}
	return basePath
}

func genQueryParams(params map[string]string) string {
	if len(params) == 0 {
		return ""
	}
	q := url.Values{}
	for k, v := range params {
		q.Add(k, v)
	}
	return q.Encode()
}

func createRepos(repo Repo, bearerToken TokenResponse, client *http.Client) (error, string) {
	repoBytes, err := json.Marshal(repo)
	if err != nil {
		fmt.Println("Failed to marshal repo: ", err)
		return err, ""
	}
	authToken := fmt.Sprintf("Bearer %s", bearerToken.Token)
	pathParams := make(map[string]string)
	pathParams["organizationId"] = organizationUuid
	req, err := buildRequest(defaultHost, http.MethodPost, createRepoUrl, pathParams, repoBytes, authToken)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(fmt.Sprintf("Import of repo %v failed: %v", repo, err))
		return err, ""
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		//handle read response error
		fmt.Println("Failed to parse body response: ", err)
		return err, ""
	}

	fmt.Printf("%s\n", string(body))
	return nil, string(body)
}

func work(file string, token TokenResponse, client * http.Client) error {
	fmt.Println("Importing json: ", file)
	jsonFile, err := os.Open(file)

	if err != nil {
		fmt.Println("Failed to load JSON file into memory: ", err)
		os.Exit(1)
	}
	defer jsonFile.Close()
	byteArr, _ := ioutil.ReadAll(jsonFile)
	var repos Repos
	if err = json.Unmarshal(byteArr, &repos); err != nil {
		fmt.Println("Failed to unmarshal JSON file: ", err)
		os.Exit(1)
	}
	fmt.Printf("%+v\n", repos)

	for _, repo := range repos.Repos {
		err, response := createRepos(repo, token, client)
		if err != nil {
			os.Exit(1)
		}
		fmt.Println("Received response from api.datenn.com: ", response)
		time.Sleep(2 * time.Second)
	}
	return nil
}

func main() {
	fmt.Println("Welcome to Datenn repo-importer script")

	var files []string

	currentDir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	reposDir := fmt.Sprintf("%s/input", currentDir)
	fmt.Printf(fmt.Sprintf("Current directory is: '%v' \nImporting json from directory '%v'\n", currentDir, reposDir))
	err = filepath.Walk(reposDir, func(path string, info os.FileInfo, err error) error {
		files = append(files, path)
		return nil
	})
	if err != nil {
		panic(err)
	}

	/*reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter Email: ")
	email, _ := reader.ReadString('\n')
	fmt.Print("Enter Password: ")
	password, _ := reader.ReadString('\n')
	fmt.Print("Enter Organization UUID: ")
	orgUuid, _ := reader.ReadString('\n')
	*/

	client := &http.Client{}
	credentials := Credentials{email, password}
	credentialsBytes, err := json.Marshal(credentials)
	if err != nil {
		panic(err)
	}
	fmt.Println("Requesting token ...")
	req, err := buildRequest(defaultHost, http.MethodPost, authenticateUrl, nil, credentialsBytes, "")
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Failed to authenticate: ", err)
		os.Exit(1)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		fmt.Println("Failed to parse authentication response: ", err)
		os.Exit(1)
	}

	//handle read response error
	if resp.StatusCode >= 300 {
		fmt.Println("Failed to authenticate: ", string(body))
		os.Exit(1)
	}
	fmt.Println(string(body))
	var token TokenResponse
	err = json.Unmarshal(body, &token)
	if err != nil {
		fmt.Println("Failed to parse Auth response from server: ", err)
		os.Exit(1)
	}

	for _, file := range files {
		if file != reposDir {
			err = work(file, token, client)
			if err != nil {
				os.Exit(1)
			}
		}
	}
}
